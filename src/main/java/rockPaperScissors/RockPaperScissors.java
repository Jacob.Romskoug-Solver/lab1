package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	public static void main(String[] args) {
        /* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

            while (true) {
                System.out.println("Let's play round " + roundCounter);
                while (true) {
                    String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
                    if (rpsChoices.contains(humanChoice)){
                        Random random = new Random();
                        int randomC = random.nextInt(rpsChoices.size());
                        String randomChoice = rpsChoices.get(randomC);                        
                        if (humanChoice.equalsIgnoreCase("rock") && randomChoice.equals("scissors")){
                            humanScore ++;
                            System.out.println("Human chose " + humanChoice + ", computer chose " + randomChoice + ". Human wins!\nScore: human " + humanScore + ", computer " + computerScore);
                            
                        } 
                        else if (humanChoice.equalsIgnoreCase("scissors") && randomChoice.equals("paper")){
                            humanScore ++;
                            System.out.println("Human chose " + humanChoice + ", computer chose " + randomChoice + ". Human wins!\nScore: human " + humanScore + ", computer " + computerScore);
                            
                        } 
                        else if (humanChoice.equalsIgnoreCase("paper") && randomChoice.equals("rock")){
                            humanScore ++;
                            System.out.println("Human chose " + humanChoice + ", computer chose " + randomChoice + ". Human wins!\nScore: human " + humanScore + ", computer " + computerScore);
                            
                        } 
                        else if (humanChoice.contains(randomChoice)){
                            System.out.println("Human chose " + humanChoice + ", computer chose " + randomChoice + ". It's a tie!\nScore: human " + humanScore + ", computer " + computerScore);

                        }
                        else {
                            computerScore ++;
                            System.out.println("Human chose " + humanChoice + ", computer chose " + randomChoice + ". Computer wins!\nScore: human " + humanScore + ", computer " + computerScore);
                        }
                        roundCounter ++;
                        break;
                    }
                }
                String keepG = readInput("Do you wish to continue playing? (y/n)?");
                if (keepG.equalsIgnoreCase("y")){
                continue;
                }
                else {
                    System.out.println("Bye bye :)");
                    break;
                }                
            
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }


}
